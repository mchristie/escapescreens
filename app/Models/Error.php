<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Error extends Model {

    protected $fillable = [
        'code',
        'level',
        'title',
    ];

    public static function activate($code) {
        Error::where('code', $code)->update(['status' => 1]);

        Event::forge('error:activated', $code);
    }

    public static function resolve($code) {

        $error = Error::where('code', $code)->first();
        $error->status = 2;
        $error->save();

        if ($error->default && Error::where('default', 1)->where('status', 2)->count() == 8) {
            Event::forge('all_errors_resolved');

        } else {
            Event::forge('error:resolved', $code);
        }

        if ($error->default && Error::where('default', 1)->where('status', 2)->count() == 6) {
            Event::forge('tensey_tense');
        }
    }

    /*
	public static function forge($code, $level = null, $title = null) {
        self::create([
            'level' => $level,
            'title' => $title,
        ]);

        Event::forge('error', $title);
    }
    */

}
