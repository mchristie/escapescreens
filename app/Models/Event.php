<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

    protected $fillable = [
        'type',
        'message'
    ];

	public static function forge($type, $message = '') {
        // Update oxygen events
        if (in_array($type, ['oxygen:draining', 'oxygen:30', 'oxygen:15', 'oxygen:5', 'oxygen:1', 'oxygen:gone'])) {
            Error::where('code', 'LIKE', 'oxygen:%')->update(['status' => 2]);
            Error::activate($type);
        }

        if ($type == 'reset') {
            \DB::table('events')->truncate();
            \DB::table('errors')->update(['status' => \DB::raw('`default`')]);
        }

        /*
        if ($type == 'activated:test') {
            Error::activate('engines');

        } else if ($type == 'deactivated:test') {
            Error::resolve('engines');
        }
        */

        return self::create([
            'type'      => $type,
            'message'   => $message,
        ]);
    }

}
