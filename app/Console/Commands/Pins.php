<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use PhpGpio\Gpio;
use \File;

class Pins extends Command {

	protected $name 		= 'pins';
	protected $description 	= 'Monitor input pins.';
	protected $pins			= [
		'boot'	=> 4
	];

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$gpio = new GPIO();

		$this->event('pins:monitoring');

		// Setup all the pins
		foreach ($this->pins as $name => $number) {
			$gpio->setup($number, 'in');
		}

		$active = 0;

		do {
			// Check for changed states
			foreach ($this->pins as $name => $number) {
				$state = $gpio->input($number);

				if ($state != $this->knownState($number)) {
					$this->info("Pin {$name} has changed to state {$state}");
					$this->knownState($number, $state);

					if ($name == 'boot' && $state != 0) {
						$this->event('boot');
					}
				}
			}

			usleep(500000);
			$active++;
		} while($active < 600);

		$this->event('pins:stopping');

	}

	public function knownState($pin, $update = null)
	{
		$states = json_decode(File::get(storage_path('pins.json')));

		if ($update !== null) {
			$states[$pin] = $update;
			File::put(storage_path('pins.json'), json_encode($states));
		}

		return $states[ $pin ];
	}

	public function event($event)
	{
		$ch = curl_init();
		$curlConfig = array(
		    CURLOPT_URL            => 'http://192.168.1.200/events',
		    // CURLOPT_URL            => 'http://gold.odyssey.app/events',
		    // CURLOPT_URL            => 'http://escape-computer.dev/events',
		    CURLOPT_POST           => true,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POSTFIELDS     => array(
		        'type' => $event
		    )
		);

		curl_setopt_array($ch, $curlConfig);
		$result = curl_exec($ch);
		$this->info('Result: '.$result);
		curl_close($ch);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['pin', InputArgument::REQUIRED, 'Pin number.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
