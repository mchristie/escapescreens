<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		// 'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		// 'Illuminate\Cookie\Middleware\EncryptCookies',
		// 'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		// 'Illuminate\Session\Middleware\StartSession',
		// 'Illuminate\View\Middleware\ShareErrorsFromSession',
		// 'App\Http\Middleware\VerifyCsrfToken',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => 'App\Http\Middleware\Authenticate',
		'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'App\Http\Middleware\RedirectIfAuthenticated',
	];

	public function handle($request)
	{
		$response = parent::handle($request);

		$response->headers->set('Access-Control-Allow-Origin', '*');
		$response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE, PUT');
		$response->headers->set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, X-Requested-With, total_results, Rebrand');
		$response->headers->set('Access-Control-Expose-Headers', 'total_results, current_page, total_pages, per_page');
		$response->headers->set('Access-Control-Allow-Credentials', 'true');

		return $response;
	}

}
