<?php

use \App\Models\Error;
use \App\Models\Event;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

if (gethostname() == 'Malcolms-MacBook-Pro.local') {
    define('SYSTEM', 'gold');

} else if (\Input::get('system')) {
    define('SYSTEM', \Input::Get('system'));

} else if (in_array(gethostname(), ['red', 'blue', 'green', 'gold'])) {
    define('SYSTEM',    gethostname());

} else {
    $host = array_get($_SERVER, 'HTTP_HOST');
    $sub = explode('.', $host);
    if (in_array($sub[0], ['red', 'blue', 'green', 'gold'])) {
        define('SYSTEM',    $sub[0]);
    } else {
        define('SYSTEM',    env('SYSTEM'));
    }
}

define('RED',       SYSTEM == 'red');
define('BLUE',      SYSTEM == 'blue');
define('GREEN',     SYSTEM == 'green');
define('GOLD',      SYSTEM == 'gold');

Route::get('start', function() {
    if (GOLD) {
        return Redirect::to('http://gold.odyssey.app/admin');

    } else {
        return Redirect::to('http://'.SYSTEM.'.odyssey.app/booting_screen');
    }
});

Route::get('start-now', function() {
    if (GOLD) {
        return Redirect::to('http://gold.odyssey.app/admin');

    } else if (RED) {
        return Redirect::to('http://'.SYSTEM.'.odyssey.app/errors_screen');

    } else if (BLUE) {
        return Redirect::to('http://'.SYSTEM.'.odyssey.app/help_screen');
    }
});

Route::get('admin', function() {
    return View::make('admin_screen');
});

Route::get('events/{limit?}', function($limit = false) {
    if ($limit) {
        return Event::orderBy('created_at', 'DESC')->take($limit)->get();
    }

    $start = time();

    do {
        $event = Event::orderBy('created_at', 'desc')->where('received_'.SYSTEM, 0)->first();

        if ($event) {
            $event->{'received_'.SYSTEM} = 1;
            $event->save();

            return $event;
        }

        $abort = (time() - $start) >= 25;
        if (!$abort) {
            sleep(1);
        }

    } while(!$abort);

    return null;
});

Route::post('events', function() {
    $event = Event::forge( Input::get('type'), Input::get('message') );

    return $event->id;
});

Route::get('errors/{display?}', function($display = null) {
    if ($display) {
        return Error::where('display', 1)->orderBy('title', 'asc')->get();
    } else {
        return Error::orderBy('id', 'asc')->get();
    }
});

Route::get('/{slug}', function($slug) {
    return View::make($slug);
});

/*
 *  Control methods
 */

Route::post('/errors/activate', function() {
    Error::activate( \Input::get('code') );
});

Route::post('/errors/resolve', function() {
    Error::resolve( \Input::get('code') );
});