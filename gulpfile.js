var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less('app.less').scripts([
        '../../bower_components/angular/angular.js',
        '../../bower_components/Chart.js/Chart.js',
        '../../bower_components/angular-chart.js/dist/angular-chart.js',
        '../../bower_components/jquery/dist/jquery.js',

        '../../bower_components/bootstrap/js/affix.js',
        '../../bower_components/bootstrap/js/alert.js',
        '../../bower_components/bootstrap/js/button.js',
        '../../bower_components/bootstrap/js/carousel.js',
        '../../bower_components/bootstrap/js/collapse.js',
        '../../bower_components/bootstrap/js/dropdown.js',
        '../../bower_components/bootstrap/js/modal.js',
        '../../bower_components/bootstrap/js/tooltip.js',
        '../../bower_components/bootstrap/js/popover.js',
        '../../bower_components/bootstrap/js/scrollspy.js',
        '../../bower_components/bootstrap/js/tab.js',
        '../../bower_components/bootstrap/js/transition.js',

        'app.js',
        'AdminController.js',
        'BootingController.js',
        'ErrorsController.js',
        'HelpController.js',
        'TabletController.js',
    ]);
});
