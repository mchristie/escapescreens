@servers(['local' => 'localhost', 'red'   => 'pi@red.odyssey.app', 'blue'  => 'pi@blue.odyssey.app', 'green' => 'pi@green.odyssey.app', 'gold'  => 'pi@gold.odyssey.app'])

@task('stop', ['on' => ['blue', 'red'], 'parallel' => true])
    {{-- sudo killall kweb3 || true --}}
    {{-- sudo killall chromium || true --}}
    {{-- sudo killall epiphany-browser || true --}}
    sudo killall firefox || true
@endtask

@task('recover', ['on' => ['blue', 'red'], 'parallel' => true])
	sudo killall firefox || true
    firefox --display=:0 localhost/start > /dev/null 2>&1 &
@endtask

@task('recover-now', ['on' => ['blue', 'red'], 'parallel' => true])
    sudo killall firefox || true
    {{-- sudo killall chromium || true --}}
    firefox --display=:0 localhost/start-now > /dev/null 2>&1 &
@endtask

@task('recover-window', ['on' => ['blue', 'red'], 'parallel' => true])
    sudo killall firefox || true
    sudo killall chromium || true
    chromium --display=:0 --incognito localhost/start-now > /dev/null 2>&1 &
@endtask


@task('gulp', ['on' => 'local'])
    gulp  --production
@endtask

@task('git', ['on' => 'local'])
    git add .
    git commit -m "Auto"
    git push origin master
@endtask

@task('update-self', ['on' => ['local']])
    git pull
@endtask

@task('update-self-all', ['on' => ['local']])
    cd ~/Desktop/EscapeScreens
    git pull
    composer update
    php artisan migrate
@endtask

@task('update-pis', ['on' => ['red', 'blue'], 'parallel' => true])
    cd /home/pi/Desktop/EscapeScreens
    git pull origin master
@endtask

@task('update-pis-all', ['on' => ['red', 'blue'], 'parallel' => true])
    cd /home/pi/Desktop/EscapeScreens
    git pull origin master
    composer update
    php artisan migrate
@endtask

@task('database-reinstall', ['on' => 'local'])
    cd ~/Desktop/EscapeScreens
    /Applications/MAMP/bin/php/php5.5.10/bin/php artisan migrate:reset --force
    /Applications/MAMP/bin/php/php5.5.10/bin/php artisan migrate --force
    /Applications/MAMP/bin/php/php5.5.10/bin/php artisan db:seed --force
@endtask

@task('reboot', ['on' => ['red', 'blue'], 'parallel' => true])
    sudo reboot -n
@endtask

@task('shutdown', ['on' => ['red', 'blue'], 'parallel' => true])
    sudo shutdown now
@endtask

{{-- Macros --}}

@macro('deploy')
    gulp
    git
@endmacro

@macro('deploy-quick')
    git
    {{-- update-quick --}}
@endmacro

@macro('update')
    update-self
    update-pis
@endmacro

@macro('update-all')
    update-self-all
    update-pis-all
@endmacro
