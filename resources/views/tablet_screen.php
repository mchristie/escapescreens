<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1, width=device-width, user-scalable=no">
    <meta charset="utf-8">
    <title>Odyssey Control</title>
    <link rel="stylesheet" href="/css/app.css">

</head>
<body ng-app="app" ng-controller="TabletController">

    <div class="container-fluid" ng-hide="showing_image">
        <div class="row">
            <div class="col-xs-12">

                <h1 class="pull-left">Odyssey G.N.C.S.</h1>

            </div>
        </div>

        <hr>

        <!--  Spacers -->

        <div class="row">
            <div class="col-xs-12">
                <h1>&nbsp;</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h1>&nbsp;</h1>
            </div>
        </div>

        <!-- Input fields -->

        <div class="row">
            <div class="col-xs-1">

            </div>

            <div class="col-xs-2" ng-click="focus(0)">
                <h1>
                    <input type="text" class="form-control text-center" id="input_0" disabled ng-model="code[0]" />
                </h1>
            </div>
            <div class="col-xs-2" ng-click="focus(1)">
                <h1>
                    <input type="text" class="form-control text-center" id="input_1" disabled ng-model="code[1]" />
                </h1>
            </div>
            <div class="col-xs-2" ng-click="focus(2)">
                <h1>
                    <input type="text" class="form-control text-center" id="input_2" disabled ng-model="code[2]" />
                </h1>
            </div>
            <div class="col-xs-2" ng-click="focus(3)">
                <h1>
                    <input type="text" class="form-control text-center" id="input_3" disabled ng-model="code[3]" />
                </h1>
            </div>
            <div class="col-xs-2" ng-click="focus(4)">
                <h1>
                    <input type="text" class="form-control text-center" id="input_4" disabled ng-model="code[4]" />
                </h1>
            </div>

            <div class="col-xs-1">

            </div>
        </div>

        <div class="row text-center">
            <div class="col-xs-12">
                <h1>&nbsp;</h1>
            </div>
            <div class="col-xs-12">
                <h3 ng-show="status == 'idle'">&nbsp;</h3>
                <h3 ng-show="status == 'verifying'">Verifying code</h3>
                <h3 ng-show="status == 'incorrect'" class="text-danger">Incorrect code</h3>
                <h3 ng-show="status == 'correct'" class="text-success">Code verified</h3>
            </div>
            <div class="col-xs-12">
                <h1>&nbsp;</h1>
            </div>
        </div>

        <!-- 1, 2, 3 -->

        <div class="row text-center">
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('1')">&nbsp; 1 &nbsp;</btn>
                </h1>
            </div>
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('2')">&nbsp; 2 &nbsp;</btn>
                </h1>
            </div>
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('3')">&nbsp; 3 &nbsp;</btn>
                </h1>
            </div>
        </div>

        <!-- 4, 5, 6 -->

        <div class="row text-center">
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('4')">&nbsp; 4 &nbsp;</btn>
                </h1>
            </div>
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('5')">&nbsp; 5 &nbsp;</btn>
                </h1>
            </div>
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('6')">&nbsp; 6 &nbsp;</btn>
                </h1>
            </div>
        </div>

        <!-- 7, 8, 9 -->

        <div class="row text-center">
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('7')">&nbsp; 7 &nbsp;</btn>
                </h1>
            </div>
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('8')">&nbsp; 8 &nbsp;</btn>
                </h1>
            </div>
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('9')">&nbsp; 9 &nbsp;</btn>
                </h1>
            </div>
        </div>

        <!-- *, 0, # -->

        <div class="row text-center">
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('clear')">&nbsp; <i class="glyphicon glyphicon-remove"></i> &nbsp;</btn>
                </h1>
            </div>
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('0')">&nbsp; 0 &nbsp;</btn>
                </h1>
            </div>
            <div class="col-xs-4">
                <h1>
                    <btn class="btn btn-lg btn-primary" ng-click="btn('submit')">&nbsp; <i class="glyphicon glyphicon-ok"></i> &nbsp;</btn>
                </h1>
            </div>
        </div>
    </div>

    <div ng-if="showing_image">
        <img src="/images/Puzzle-Stage-1-CYE.png" style="width: 100%;" />
    </div>

    <audio src="/sounds/Computer_Data_03.wav" id="audio" autoplay></audio>

    <audio src="/sounds/Computer_Data_01.wav" id="type" preload></audio>
    <audio src="/sounds/Computer_Data_05.wav" id="incorrect" preload></audio>

    <script src="/js/all.js"></script>

    <script>

        // var SYSTEM  = '<?= SYSTEM ?>';
        var SYSTEM  = 'green';
        var RED     = SYSTEM == 'red';
        var BLUE    = SYSTEM == 'blue';
        var GREEN   = SYSTEM == 'green';
        var GOLD    = SYSTEM == 'gold';
    </script>

</body>
</html>