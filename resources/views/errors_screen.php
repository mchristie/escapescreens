<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1, width=device-width, user-scalable=no">
    <meta charset="utf-8">
    <title>Odyssey Control</title>
    <link rel="stylesheet" href="/css/app.css">

</head>
<body ng-app="app" ng-controller="ErrorsController" style="padding: 10px 50px;" ng-class="{'lock-down': showing_lockdown, 'launched': showing_launched}">

    <div class="container-fluid">
	    <div class="row">
		    <div class="col-md-12">

			    <h1 class="pull-left">
                    Odyssey Control
                </h1>

                <!-- Clock is always hidden now -->
			    <!-- <h1 class="pull-right text-danger blink" ng-show="minutes != null && !showing_lockdown">{{minutes}}:{{seconds < 10 ? '0'+seconds : seconds}} mins oxygen remaining</h1> -->

                <!--
                <p class="pull-right">
                    <button ng-click="fireEvent('launching')">Launch</button>
                    <button ng-click="fireEvent('lockdown')">Lockdown</button>
                </p>
                -->

		    </div>
	    </div>

	    <hr>

        <div class="row" ng-hide="showing_webcam || showing_message || showing_launching || showing_failed || showing_exit_code || showing_lockdown || showing_launched">

            <div class="col-md-3" ng-repeat="error in errors">
                <div class="panel" ng-class="{'panel-danger': error.status == 1, 'panel-success': error.status == 2}">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <div class="row">
                            <span class="alt-font col-sm-2" style="font-size: 2em;">{{::error.id}}:</span>
                            <span class="col-sm-10"> {{::error.title}}</span>
                        </h3>
                    </div>
                    <div class="panel-body text-center">
                        <img ng-src="/images/{{error.image}}"
                             style="width: 80%;"
                             ng-class="{'faded': error.status == 2}"
                            />
                    </div>
                </div>
            </div>

        </div>

        <!--

	    <div class="row" ng-if="!showing_webcam && !showing_message && !showing_launching && !showing_failed">
	    	<div class="col-md-6">

	    		<h4>Error messages</h4>
                <hr />

	    		<dl ng-repeat="error in errors">
		    		<dt class="text-{{error.level}} blink text-center">- {{error.level}} -</dt>
		    		<dd class="text-{{error.level}}">
		    			{{error.title}}
		    		</dd>
                    <hr />
		    	</dl>

	    		<div class="text-success" ng-hide="errors.length">
		    		No errors in progress
	    		</div>

	    	</div>
		    <div class="col-md-6">

                <div ng-if="graph == 'power'">
    		    	<h4>Power Consumption</h4>
                    <hr />

    		    	<canvas id="power" class="chart chart-radar" data="power.data" labels="power.labels" options="power.options"></canvas>
                </div>

                <div ng-if="graph == 'hydraulics'">
                    <h4>Hydraulic Pressure</h4>
                    <hr />

                    <canvas id="hydraulics" class="chart chart-bar" data="hydraulics.data" labels="hydraulics.labels" options="hydraulics.options"></canvas>
                </div>

                <div ng-if="graph == 'air'">
                    <h4>Air Composition</h4>
                    <hr />

                    <canvas id="air" class="chart chart-bar" data="air.data" labels="air.labels" options="air.options"></canvas>
                </div>

		    </div>
	    </div>

        -->


        <webcam ng-if="showing_webcam"></webcam>

        <video-message ng-if="showing_message"></video-message>

        <launching ng-if="showing_launching"></launching>

        <exit-code ng-if="showing_exit_code"></exit-code>

        <lockdown ng-if="showing_lockdown" minutes="minutes" seconds="seconds"></lockdown>

        <failed ng-if="showing_failed"></failed>

        <launched ng-if="showing_launched"></launched>

    </div>

    <audio src="/sounds/Computer_Data_02.wav" id="audio" autoplay></audio>

    <!-- <sound></sound> -->

    <script src="/js/all.js"></script>

    <script>
        var SYSTEM  = '<?= SYSTEM ?>';
        var RED     = SYSTEM == 'red';
        var BLUE    = SYSTEM == 'blue';
        var GREEN   = SYSTEM == 'green';
        var GOLD    = SYSTEM == 'gold';
    </script>

</body>
</html>