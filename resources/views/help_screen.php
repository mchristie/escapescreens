<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1, width=device-width, user-scalable=no">
    <meta charset="utf-8">
    <title>Odyssey Control</title>
    <link rel="stylesheet" href="/css/app.css">

</head>
<body ng-app="app" ng-controller="HelpController" style="padding: 10px 50px;" ng-class="{'lock-down': showing_lockdown, 'launched': showing_launched}">

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <h1 class="pull-left">Odyssey Interactive Support System</h1>

            </div>
        </div>

        <hr>

        <div ng-hide="showing_lockdown || showing_launched || showing_exit_code || showing_failed">

            <div class="row" id="sine-container" ng-hide="state == 'offline' || (state == 'intermittent' && !flicker)">
                <div class="col-md-12" style="padding-top: 5%; padding-top: 5%;">

                    <canvas id="sine" style="width: 100%; height: 100%;" width="800" height="600"></canvas>

                    <h1 class="text-center" id="help-message" ng-show="message != 0">{{message}}</h1>

                </div>
            </div>

            <div class="row" id="sine-container" ng-show="state == 'offline' || (state == 'intermittent' && !flicker)">
                <div class="col-md-12 text-center" style="padding-top: 20%;">

                    <h2>O.I.S.S. is offline</h2>

                </div>
            </div>

            <div class="row" ng-hide="state == 'offline' || (state == 'intermittent' && !flicker)">
                <div class="col-md-12 text-center">
                    <form ng-submit="ask()">
                        <hr />
                        <h1>
                            <input type="text" id="question" ng-model="question" placeholder="{{prompt}}" />
                        </h1>
                        <!-- <input type="submit"> -->
                    </form>
                </div>
                <!--
                <div class="col-md-2 text-center">
                    <button class="btn btn-sm btn-primary">Ask</button>
                </div>
                -->
            </div>
        </div>

        <div ng-show="showing_lockdown" class="text-center">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel" style="background-color: #000;">
                        <img src="/images/lockdown_puzzle.png" style="margin: 20px 0;" />
                    </div>
                </div>
            </div>
            <!--
            <div class="row">
                <div class="col-md-12">
                    <h1>&nbsp;</h1>
                    <h2 class="huge">Manual override needed</h2>
                    <! - - <h3>Enter authorisation codes to disable lockdown</h3> - - >
                    <h1>&nbsp;</h1>
                    <h1 class="huge">
                        <i class="glyphicon" ng-class="{'glyphicon-record': !code_one_done, 'glyphicon-ok-sign': code_one_done}"></i>
                        <i class="glyphicon" ng-class="{'glyphicon-record': !code_two_done, 'glyphicon-ok-sign': code_two_done}"></i>
                        <i class="glyphicon" ng-class="{'glyphicon-record': !code_three_done, 'glyphicon-ok-sign': code_three_done}"></i>
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group">
                        <! - - <label>Stage one code</label> - - >
                        <input type="text"
                                id="code_one"
                                class="form-control alt-font huge"
                                placeholder="Stage one code"
                                ng-model="code_one"
                                ng-hide="code_one_done == null || code_one_done == true"
                                ng-change="checkCode('one')"
                            >
                    </div>
                    <div class="form-group">
                        <! - - <label>Stage two code</label> - - >
                        <input type="text"
                                id="code_two"
                                class="form-control alt-font huge"
                                placeholder="Stage two code"
                                ng-model="code_two"
                                ng-hide="code_two_done == null || code_two_done == true"
                                ng-change="checkCode('two')"
                            >
                    </div>
                    <div class="form-group">
                        <! - - <label>Stage three code</label> - - >
                        <input type="text"
                                id="code_three"
                                class="form-control alt-font huge"
                                placeholder="Stage three code"
                                ng-model="code_three"
                                ng-hide="code_three_done == null || code_three_done == true"
                                ng-change="checkCode('three')"
                            >
                    </div>
                </div>
            </div>
            -->
        </div>

    </div>

    <exit-code ng-if="showing_exit_code"></exit-code>

    <launched ng-if="showing_launched"></launched>

    <failed ng-if="showing_failed"></failed>

    <audio src="/sounds/Computer_Data_03.wav" id="audio" autoplay></audio>

    <script src="/js/all.js"></script>

    <script>
        var SYSTEM  = '<?= SYSTEM ?>';
        var RED     = SYSTEM == 'red';
        var BLUE    = SYSTEM == 'blue';
        var GREEN   = SYSTEM == 'green';
        var GOLD    = SYSTEM == 'gold';
    </script>

</body>
</html>