<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1, width=device-width, user-scalable=no">
    <meta charset="utf-8">
    <title>Odyssey Control</title>
    <link rel="stylesheet" href="/css/app.css">

</head>
<body ng-app="app">

    <div class="container-fluid" ng-controller="AdminController">
        <div class="row">
            <div class="col-md-12">

                <h1 class="pull-left">Odyssey Control</h1>

                <h1 class="pull-right">
                    <i class="glyphicon glyphicon-play" ng-click="startTimer()" ng-hide="timing"></i>
                    <i class="glyphicon glyphicon-stop" ng-click="stopTimer()" ng-show="timing"></i>

                    <i class="glyphicon glyphicon-forward" ng-click="changeTimer()" ng-show="timing"></i>

                    <span ng-class="{'text-warning': seconds < 300 && seconds >= 60, 'text-danger': seconds < 60}">{{time}}</span>
                </h1>

            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-6">

                <h4>Error statuses &nbsp; <i class="glyphicon" ng-class="{'glyphicon-refresh': !updating_errors, 'glyphicon-time': updating_errors}" ng-click="updateErrors()"></i></h4>
                <hr />

                <table class="table">
                    <tr ng-repeat="error in errors">
                        <td>
                            <button class="btn btn-sm btn-danger" ng-show="error.status == 1" ng-click="toggleError(error);">
                                Resolve
                            </button>

                            <button class="btn btn-sm btn-success" ng-show="error.status == 2" ng-click="toggleError(error);">
                                Activate
                            </button>

                            <button class="btn btn-sm btn-info" ng-show="error.status == 0" ng-click="toggleError(error);">
                                Activate
                            </button>
                        </td>
                        <td>
                            {{error.title}}
                        </td>
                    </tr>
                </table>

            </div>
            <div class="col-md-6">

                <h4>Recent Events &nbsp;
                    <i class="glyphicon" ng-class="{'glyphicon-refresh': !updating_events, 'glyphicon-time': updating_events}" ng-click="updateEvents()"></i>

                    <span class="pull-right">

                        <i class="glyphicon glyphicon-exclamation-sign" ng-click="fireEvent('ping')"></i>

                        &nbsp;

                        <span ng-show="status_gold" data-toggle="tooltip" data-placement="top" title="Gold server is OK">
                            <i class="glyphicon glyphicon-ok"></i>
                        </span>
                        <span ng-hide="status_gold" data-toggle="tooltip" data-placement="top" title="Gold server has not responded" style="color: red;">
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>

                        <span ng-show="status_red" data-toggle="tooltip" data-placement="top" title="Red server is OK">
                            <i class="glyphicon glyphicon-ok"></i>
                        </span>
                        <span ng-hide="status_red" data-toggle="tooltip" data-placement="top" title="Red server has not responded" style="color: red;">
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>

                        <span ng-show="status_blue" data-toggle="tooltip" data-placement="top" title="Blue server is OK">
                            <i class="glyphicon glyphicon-ok"></i>
                        </span>
                        <span ng-hide="status_blue" data-toggle="tooltip" data-placement="top" title="Blue server has not responded" style="color: red;">
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>

                        <span ng-show="status_green" data-toggle="tooltip" data-placement="top" title="Green server is OK">
                            <i class="glyphicon glyphicon-ok"></i>
                        </span>
                        <span ng-hide="status_green" data-toggle="tooltip" data-placement="top" title="Green server has not responded" style="color: red;">
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>
                    </span>
                </h4>
                <hr />

                <ul>
                    <li ng-repeat="event in events track by $index" ng-class="{'text-warning': highlight_events.indexOf(event.type) != -1}">
                        {{event.type}}
                        <span ng-if="event.message"> - {{event.message}}</span>
                    </li>
                </ul>

                <hr />

                <h4>
                    <span class="pull-left">
                        Events
                    </span>

                    <span class="pull-right" style="margin-bottom: 5px;">
                        <button class="btn btn-xs btn-info"
                            ng-click="fire_event = 'help:active'; $('#event_message').focus();">
                            O.I.S.S.
                        </button>
                        &nbsp;
                        <button class="btn btn-xs"
                            ng-class="{'btn-info': !webcam, 'btn-danger': webcam}"
                            ng-click="fire_event = webcam ? 'webcam:deactivate' : 'webcam:activate';">
                            Webcam
                        </button>
                        &nbsp;
                        <button class="btn btn-xs"
                            ng-class="{'btn-info': !video_message, 'btn-danger': video_message}"
                            ng-click="fire_event = video_message ? 'video_message:stop' : 'video_message:start';">
                            Video Msg
                        </button>
                        &nbsp;
                        <button class="btn btn-xs btn-info"
                            ng-click="fire_event = 'exit'">
                            Exit
                        </button>
                    </span>
                </h4>

                <hr style="clear: both;" />

                <div class="row">
                    <div class="col-md-8">
                        <select class="form-control" ng-model="fire_event">
                            <option value="">- Select -</option>
                            <optgroup label="General">
                                <option value="boot">Boot</option>
                                <option value="reset">Reset</option>
                                <option value="all_errors_resolved">All Errors Resolved</option>
                                <option value="launching">Begin launch process</option>
                                <option value="lockdown">Lockdown</option>
                                <option value="failed">Failed</option>
                                <option value="launched">Launched!</option>
                                <option value="exit">Exit code</option>
                                <option value="clear">Clear</option>
                                <option value="ping">Ping</option>
                                <option value="timer">Timer</option>
                            </optgroup>
                            <optgroup label="L.O.I.S.S.">
                                <option value="help:voice|errors_resolved.wav">All Errors Resolved</option>
                                <option value="help:voice|even_trying.wav">Even Trying</option>
                                <option value="help:voice|lives_at_risk.wav">Lives At Risk</option>
                                <option value="help:voice|look_closely.wav">Look Closely</option>
                                <option value="help:voice|not_right.wav">Not Right</option>
                            </optgroup>
                            <optgroup label="Help screen">
                                <option value="help:offline">Offline</option>
                                <option value="help:active">Show Message</option>
                                <option value="help:intermittent">Intermittent</option>
                                <option value="help:idle">Idle</option>
                                <option value="help:request">Help request</option>
                            </optgroup>
                            <optgroup label="Webcam">
                                <option value="webcam:activate">Webcam - Show</option>
                                <option value="webcam:deactivate">Webcam - Hide</option>
                            </optgroup>
                            <optgroup label="Video_message">
                                <option value="video_message:start">Video - Show</option>
                                <option value="video_message:stop">Video - Hide</option>
                            </optgroup>
                            <optgroup label="G.N.C.S">
                                <option value="gncs:correct">GNCS Correct</option>
                                <option value="gncs:incorrect">GNCS Incorrect</option>
                            </optgroup>
                            <optgroup label="Oxygen">
                                <option value="oxygen:draining">Oxygen - draining</option>
                                <option value="oxygen:30">Oxygen - 30 mins</option>
                                <option value="oxygen:5">Oxygen - 5 mins</option>
                                <option value="oxygen:1">Oxygen - 1 mins</option>
                                <option value="oxygen:gone">Oxygen - gone</option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-sm btn-info" ng-click="fireEvent()">Fire!</button>
                    </div>
                </div>

                <div class="row" ng-show="fire_event == 'help:active'">
                    <div class="col-md-12" style="margin-top: 10px;">
                        <select class="form-control" ng-model="event_message">
                            <option value="">Sample messages (or type your own)</option>
                            <option>That is the final error corrected agents, I now need the final launch code?</option>
                            <option>Virgo looks good</option>
                            <option>Examine everything closely</option>
                            <option># Oh Watson</option>
                        </select>
                    </div>
                </div>

                <div class="row" ng-show="fire_event == 'help:active' || fire_event == 'timer'">
                    <div class="col-md-12" style="margin-top: 10px;">
                        <input class="form-control" placeholder="Event_message" ng-model="event_message" id="event_message" />
                    </div>
                </div>

            </div>

        </div>
    </div>

    <audio src="/sounds/Computer_Data_02.wav" id="audio" autoplay></audio>

    <script src="/js/all.js"></script>

    <script>
        var SYSTEM  = '<?= SYSTEM ?>';
        var RED     = SYSTEM == 'red';
        var BLUE    = SYSTEM == 'blue';
        var GREEN   = SYSTEM == 'green';
        var GOLD    = SYSTEM == 'gold';

        $('body').tooltip({});
    </script>

</body>
</html>