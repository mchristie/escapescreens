<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1, width=device-width, user-scalable=no">
    <meta charset="utf-8">
    <title>Odyssey Control</title>
    <link rel="stylesheet" href="/css/app.css">

</head>
<body ng-app="app" style="background-color: #000;">

    <div class="container-fluid" ng-controller="BootingController">
        <div class="row">
            <div class="col-md-12">

            <div id="booting-text">
                <h1 style="margin-top: 20%;" class="text-center text-danger">Insufficient Power</h1>
                <h2 class="text-center text-danger">Restore power to start evacuation</h2>
            </div>
        </div>
    </div>

    <audio src="/sounds/buzzer.wav" id="audio" autoplay></audio>

    <script src="/js/all.js"></script>

    <script>
        var SYSTEM  = '<?= SYSTEM ?>';
        var RED     = SYSTEM == 'red';
        var BLUE    = SYSTEM == 'blue';
        var GREEN   = SYSTEM == 'green';
        var GOLD    = SYSTEM == 'gold';
    </script>

</body>
</html>