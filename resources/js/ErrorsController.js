app.controller('ErrorsController', ['$scope', 'server', '$timeout', '$interval', 'sounds', function($scope, server, $timeout, $interval, sounds) {

    /*
     *  Errors
     */

    var clearShowing = function() {
        $scope.showing_webcam       = false;
        $scope.showing_message      = false;
        $scope.showing_launching    = false;
        $scope.showing_exit_code    = false;
        $scope.showing_failed       = false;
        $scope.showing_lockdown     = false;
        $scope.showing_launched     = false;

        $scope.launching_failed     = false;
    }
    clearShowing();

    $scope.fireEvent = function(evt) {
        server.fireEvent(evt);
    }

     // Update the errors list
    server.errors(function(errors) {
        $scope.errors = errors;
    }, 1);

    $scope.minutes = null;
    $scope.seconds = 57;

    $interval(function() {
        $('.blink').addClass('invisible');

        if ($scope.seconds > 0) {
            $scope.seconds--;
        }

        setTimeout(function() {
            $('.blink').removeClass('invisible');
        }, 250);
    }, 1000);


    server.eventListener(function(evt) {
        if (evt.type == 'reset') {
            window.location.href = '/booting_screen';
            return;
        }

        // Always update the errors list, just in case
        // if (evt.type == 'error:activated' || evt.type == 'error:resolved') {
            server.errors(function(errors) {
                $scope.errors = errors;
            }, 1);
        // }

        if (evt.type == 'timer') {
            $scope.minutes = evt.message;
            $scope.seconds = 57;
        }

        // Toggle the webcam
        if (evt.type == 'webcam:activate') {
            clearShowing();
            $scope.showing_webcam = true;
            sounds.play('loiss_active');
        }
        if (evt.type == 'webcam:deactivate') {
            clearShowing();
            $scope.showing_webcam = false;
        }

        // Toggle the video message
        if (evt.type == 'video_message:start') {
            clearShowing();
            $scope.showing_message = true;
            sounds.play('incoming_message.wav', true);
            setTimeout(function() {
                $('#video')[0].play();
            }, 1000);
        }
        if (evt.type == 'video_message:stop') {
            clearShowing();
            $scope.showing_message = false;
        }

        // Toggle end states
        if (evt.type == 'launching') {
            clearShowing();
            $scope.showing_launching = true;

            var pc = 0;
            var stop = $interval(function() {
                pc += 7;
                $('#launching div').css('width', pc+'%');

                if (pc >= 70) {
                    $interval.cancel(stop);
                    $scope.launching_failed = true;
                    $scope.$apply();

                    // server.activateError('engine_room');
                    server.fireEvent('lockdown');
                }

            }, 1000);
        }
        if (evt.type == 'lockdown') {
            clearShowing();
            $scope.showing_lockdown = true;
        }
        if (evt.type == 'exit') {
            clearShowing();
            $scope.showing_exit_code = true;
            // sounds.play('evacuate');
        }
        if (evt.type == 'failed') {
            clearShowing();
            $scope.showing_failed = true;
            sounds.play('failed');
        }
        if (evt.type == 'launched') {
            clearShowing();
            $scope.showing_launched = true;
        }
        if (evt.type == 'clear') {
            clearShowing();
        }
    });

    /*
     *  Power chart
     */

    // $scope.graph = ['hydraulics', 'power', 'air'][ Math.round( Math.random() ) ];
    $scope.graph = 'air';
    var chart_change = 0;

    $scope.power = {
        labels: ["Engines", "Life Support", "Control", "Heating", "Experiments", "Orbit Control"],
        data: [
            [65, 59, 90, 81, 56, 98],
            // [28, 48, 40, 19, 96, 86]
        ],
        options: {
            animationSteps: 30,
            showTooltips: false,
            pointLabelFontColor: '#DDD',
            angleLineColor: '#666',
            pointLabelFontFamily:'space',
            scaleBeginAtZero: true,
            scaleOverride: true,
            scaleSteps: 10,
            scaleStepWidth: 10,
            scaleStartValue: 0,
        }
    }

    $scope.hydraulics = {
        labels: ['EVR', 'PVR', 'Reactor', 'MARMD', 'Candarm'],
        data: [[65, 59, 80, 81, 56]],
        series: ['Hydraulics'],
        options: {
            animationSteps: 30,
            showTooltips: false,
            scaleFontColor: '#DDD',
            scaleLineColor: '#666',
            scaleFontFamily: 'space',
            scaleBeginAtZero: true,
            scaleOverride: true,
            scaleSteps: 10,
            scaleStepWidth: 10,
            scaleStartValue: 0,
        }
    }

    $scope.air = {
        // labels: ['Nitrogen', 'Oxygen', 'Argon', 'Carbon Dioxide', 'Methane'],
        labels: ['N7', 'O2', 'AR18', 'CO2', 'CH4'],
        data: [[57, 20, 1, 15, 5]],
        series: ['Components'],
        options: {
            animationSteps: 30,
            showTooltips: false,
            scaleFontColor: '#DDD',
            scaleLineColor: '#666',
            scaleFontFamily: 'space',
            scaleBeginAtZero: true,
            scaleOverride: true,
            scaleSteps: 10,
            scaleStepWidth: 6,
            scaleStartValue: 0,
        }
    }

    setInterval(function() {
        if ($scope.graph == 'power') {

            // for (var i = 0; i < 2; i++) {
                var i = 0;
                for (var j = 0; j < $scope.power.data[i].length; j++) {

                    dir = Math.round(Math.random());
                    amount = Math.round(Math.random() * 5);

                    if ((dir && $scope.power.data[i][j] > 10) || $scope.power.data[i][j] >= 100) {
                        $scope.power.data[i][j] -= amount;
                    } else {
                        $scope.power.data[i][j] += amount;
                    }
                }
            // }

        } else if ($scope.graph == 'hydraulics') {
            var i = 0;
            for (var j = 0; j < $scope.hydraulics.data[i].length; j++) {

                dir = Math.round(Math.random());
                amount = Math.round(Math.random() * 5);

                if ((dir && $scope.hydraulics.data[i][j] > 10) || $scope.hydraulics.data[i][j] >= 100) {
                    $scope.hydraulics.data[i][j] -= amount;
                } else {
                    $scope.hydraulics.data[i][j] += amount;
                }
            }

        } else if ($scope.graph == 'air') {
            // Change Oxygen levels
            var o2 = 20 * ((100 / 60 * $scope.minutes) / 100);
            $scope.air.data[0][1] = o2;
            $scope.air.data[0][3] = 1 + (20 - o2);
        }

        chart_change++;
        if (chart_change >= 60) {
            if ($scope.graph == 'power') {
                $scope.graph = 'hydraulics';

            } else if ($scope.graph == 'hydraulics') {
                $scope.graph = 'air';

            } else if ($scope.graph == 'air') {
                $scope.graph = 'power';
            }

            chart_change = 0;
        }

        $scope.$apply();
    }, 1500);

}]).directive('webcam', function() {
    return {
        controller: ['$scope', '$interval', function($scope, $interval) {
            $interval(function() {
                $scope.url = 'http://192.168.1.204:99/snapshot.cgi?user=admin&pwd=&' + new Date().getTime() + Math.random();
            }, 100);
        }],
        template: '<div class="row">\
            <div class="col-md-8 col-md-offset-2">\
                <div class="panel panel-primary">\
                    <div class="panel-heading">\
                        <h3 class="panel-title">Life Support System</h3>\
                    </div>\
                    <div class="panel-body text-center">\
                        <img ng-src="{{url}}" />\
                    </div>\
                </div>\
            </div>\
        </div>'
    };

}).directive('videoMessage', function() {
    return {
        template: '<div class="row">\
            <div class="col-md-8 col-md-offset-2">\
                <div class="panel panel-primary">\
                    <div class="panel-heading">\
                        <h3 class="panel-title">Incoming message - Lunar One</h3>\
                    </div>\
                    <div class="panel-body text-center">\
                        <video src="/message.m4v" preload="none" id="video"></video>\
                    </div>\
                </div>\
            </div>\
        </div>'
    };

}).directive('launching', function() {
    return {
        template: '<div class="row">\
            <div class="col-md-12 text-center">\
                <h1>&nbsp;</h1>\
                <h1>&nbsp;</h1>\
                <h1 class="blink text-success" ng-hide="launching_failed">Initiating launch procedure</h1>\
                <h1 class="blink text-danger" ng-show="launching_failed">Launch failure</h1>\
                <div class="progress" id="launching" style="margin-top: 50px; margin-left: 15%; width: 70%;">\
                    <div class="progress-bar progress-bar-striped" ng-class="{\'progress-bar-success\': !launching_failed, \'progress-bar-danger\': launching_failed, \'active\': !launching_failed}" style="width: 0%;"></div>\
                </div>\
            </div>\
        </div>'
    };

}).directive('exitCode', function() {
    return {
        template: '<div class="row">\
            <div class="col-md-12 text-center">\
                <h1>&nbsp;</h1>\
                <h1>&nbsp;</h1>\
                <h1>&nbsp;</h1>\
                <h1 class="text-success">Manual Override successful</h1>\
                <h2 style="margin-top: 100px;">\
                    Emergency Launch Code is\
                </h2>\
                <h1>20 0B 98</h1>\
            </div>\
        </div>'
    };

}).directive('lockdown', function() {
    return {
        scope: {
            minutes: '=',
            seconds: '=',
        },
        template: '<div class="row">\
            <div class="col-md-12 text-center">\
                <h1>&nbsp;</h1>\
                <h2>&nbsp;</h2>\
                <h2 class="text-danger blink">Lockdown</h2>\
                <h2 class="text-danger blink">Launch Failure</h2>\
            </div>\
        </div>'
    };
    // <h2 class="text-danger blink">Oxygen remaining: {{minutes}}:{{seconds < 10 ? \'0\'+seconds : seconds}}</h2>\

}).directive('failed', function() {
    return {
        template: '<div class="row">\
            <div class="col-md-12 text-center">\
                <h1>&nbsp;</h1>\
                <h1>&nbsp;</h1>\
                <h1 class="text-danger">Mission Failed</h1>\
                <h1 class="text-danger">Impact Imminent</h1>\
            </div>\
        </div>'
    };
}).directive('launched', function() {
    return {
        template: '<div class="row">\
            <div class="col-md-12 text-center">\
                <h1>&nbsp;</h1>\
                <h1>&nbsp;</h1>\
                <h1 class="text-danger">Launch Sequence Complete</h1>\
                <h1>&nbsp;</h1>\
                <h1 class="text-danger">Mission Successful!</h1>\
            </div>\
        </div>'
    };
});