app.controller('BootingController', ['$scope', 'server', 'sounds', '$interval', function($scope, server, sounds, $interval) {
    var lines = [
        {delay: 1000, line: 'Initialising boot sequence...'},
        {delay: 1000, line: 'Connecting to network storage provider...'},
        {delay: 0, line: 'Connected.'},
        {delay: 1000, line: 'Downloading operating system bootstraper...'},
        {delay: 500, line: 'Downloading [>----------------------------------] 0%'},
        {delay: 200, replace: 'Downloading [==>--------------------------------] 5%'},
        {delay: 200, replace: 'Downloading [====>------------------------------] 10%'},
        {delay: 200, replace: 'Downloading [======>----------------------------] 25%'},
        {delay: 200, replace: 'Downloading [========>--------------------------] 30%'},
        {delay: 200, replace: 'Downloading [==========>------------------------] 35%'},
        {delay: 200, replace: 'Downloading [============>----------------------] 40%'},
        {delay: 200, replace: 'Downloading [==============>--------------------] 45%'},
        {delay: 200, replace: 'Downloading [================>------------------] 50%'},
        {delay: 200, replace: 'Downloading [==================>----------------] 55%'},
        {delay: 200, replace: 'Downloading [====================>--------------] 60%'},
        {delay: 200, replace: 'Downloading [======================>------------] 65%'},
        {delay: 200, replace: 'Downloading [========================>----------] 70%'},
        {delay: 200, replace: 'Downloading [==========================>--------] 75%'},
        {delay: 200, replace: 'Downloading [============================>------] 80%'},
        {delay: 200, replace: 'Downloading [==============================>----] 85%'},
        {delay: 200, replace: 'Downloading [================================>--] 90%'},
        {delay: 200, replace: 'Downloading [==================================>] 95%'},
        {delay: 200, replace: 'Downloading [====================================] 100%'},
        {delay: 200, line: '&nbsp;'},
        {delay: 1000, line: 'Unzipping OS--2014_04_21--DM02234.gzip ...'},
        {delay: 300, line: 'Unzipping [>----------------------------------] 0%'},
        {delay: 100, replace: 'Unzipping [==>--------------------------------] 5%'},
        {delay: 100, replace: 'Unzipping [====>------------------------------] 10%'},
        {delay: 100, replace: 'Unzipping [======>----------------------------] 25%'},
        {delay: 100, replace: 'Unzipping [========>--------------------------] 30%'},
        {delay: 100, replace: 'Unzipping [==========>------------------------] 35%'},
        {delay: 100, replace: 'Unzipping [============>----------------------] 40%'},
        {delay: 100, replace: 'Unzipping [==============>--------------------] 45%'},
        {delay: 100, replace: 'Unzipping [================>------------------] 50%'},
        {delay: 100, replace: 'Unzipping [==================>----------------] 55%'},
        {delay: 100, replace: 'Unzipping [====================>--------------] 60%'},
        {delay: 100, replace: 'Unzipping [======================>------------] 65%'},
        {delay: 100, replace: 'Unzipping [========================>----------] 70%'},
        {delay: 100, replace: 'Unzipping [==========================>--------] 75%'},
        {delay: 100, replace: 'Unzipping [============================>------] 80%'},
        {delay: 100, replace: 'Unzipping [==============================>----] 85%'},
        {delay: 100, replace: 'Unzipping [================================>--] 90%'},
        {delay: 100, replace: 'Unzipping [==================================>] 95%'},
        {delay: 100, replace: 'Unzipping [====================================] 100%'},
        {delay: 100, line: '&nbsp;'},
        {delay: 100, line: 'Preparing to boot...'},
        {delay: 300, line: 'Installing &nbsp; &nbsp; com.lib.graphics.vectors'},
        {delay: 300, line: 'Installing &nbsp; &nbsp; com.lib.graphics.fonts'},
        {delay: 300, line: 'Installing &nbsp; &nbsp; com.lib.graphics.bitmaps'},
        {delay: 300, line: 'Installing &nbsp; &nbsp; com.lib.graphics.sprites'},
        {delay: 300, line: 'Installing &nbsp; &nbsp; com.lib.graphics.videos'},
        {delay: 300, line: 'Installing &nbsp; &nbsp; com.lib.audio.alerts'},
        {delay: 300, line: 'Installing &nbsp; &nbsp; com.lib.audio.speech'},
        {delay: 300, line: 'Installing &nbsp; &nbsp; com.lib.audio.uncompressed'},
        {delay: 300, line: 'Installing &nbsp; &nbsp; com.lib.audio.midi'},
        {delay: 300, line: 'Installing &nbsp; &nbsp; com.lib.audio.m4a'},

        {delay: 500, line: ' &nbsp;'},
        {delay: 500, line: 'Connecting to additional systems...'},
        {delay: 500, line: '. . . . . . . . power control'},
        {delay: 500, line: '. . . . . . . . environmentals'},
        {delay: 500, line: '. . . . . . . . altitude and orbital stability operations'},
        {delay: 500, line: '. . . . . . . . communication systems'},
        {delay: 500, line: '. . . . . . . . life support'},
        {delay: 500, line: '. . . . . . . . operational CTR'},
        {delay: 500, line: '. . . . . . . . networking control'},
        {delay: 500, line: '. . . . . . . . emergency monitoring'},
        {delay: 500, line: '. . . . . . . . internal systems'},
        {delay: 500, line: ' &nbsp;'},

        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Accessibility'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Accounts'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Assistant'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Automator'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; BridgeSupport'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; CacheDelete'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Caches'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; ColorSync'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Components'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Compositions'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; ConfigurationProfiles'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; CoreServices'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; CryptoTokenKit'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; DirectoryServices'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Displays'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; DTDs'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Extensions'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Filesystems'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Filters'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Frameworks'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Graphics'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; IdentityServices'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Image Capture'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Input Methods'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Java'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; KerberosPlugins'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Kernels'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Keyboard Layouts'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Keychains'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; LaunchAgents'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; LaunchDaemons'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; LinguisticData'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; LocationBundles'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; LoginPlugins'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Metadata'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; MonitorPanels'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; OpenDirectory'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; OpenSSL'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Packages'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Password Server Filters'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; PerformanceMetrics'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Perl'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; PreferencePanes'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; PrelinkedKernels'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; PrivateFrameworks'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; QuickLook'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; QuickTime'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Recents'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Sandbox'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Screen Savers'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; ScreenReader'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; ScriptingAdditions'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; ScriptingDefinitions'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; SDKSettingsPlist'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Security'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Services'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Sounds'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Speech'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Spelling'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Spotlight'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; StagedFrameworks'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; StartupItems'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; SyncServices'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; SystemConfiguration'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; SystemProfiler'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Tcl'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; TextEncodings'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; User Template'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; UserEventPlugins'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; Video'},
        {delay: 200, line: 'Initialising... &nbsp; &nbsp; WidgetResources'},

        {delay: 1000, line: ' &nbsp;'},
        {delay: 1000, line: 'Initialising graphical interface...'},
        {delay: 1000, line: ' &nbsp;'},

        {delay: 300, line: 'Booting [>----------------------------------] 0%'},
        {delay: 300, replace: 'Booting [==>--------------------------------] 5%'},
        {delay: 300, replace: 'Booting [====>------------------------------] 10%'},
        {delay: 300, replace: 'Booting [======>----------------------------] 25%'},
        {delay: 300, replace: 'Booting [========>--------------------------] 30%'},
        {delay: 300, replace: 'Booting [==========>------------------------] 35%'},
        {delay: 300, replace: 'Booting [============>----------------------] 40%'},
        {delay: 300, replace: 'Booting [==============>--------------------] 45%'},
        {delay: 300, replace: 'Booting [================>------------------] 50%'},
        {delay: 300, replace: 'Booting [==================>----------------] 55%'},
        {delay: 300, replace: 'Booting [====================>--------------] 60%'},
        {delay: 300, replace: 'Booting [======================>------------] 65%'},
        {delay: 300, replace: 'Booting [========================>----------] 70%'},
        {delay: 300, replace: 'Booting [==========================>--------] 75%'},
        {delay: 300, replace: 'Booting [============================>------] 80%'},
        {delay: 300, replace: 'Booting [==============================>----] 85%'},
        {delay: 300, replace: 'Booting [================================>--] 90%'},
        {delay: 300, replace: 'Booting [==================================>] 95%'},
        {delay: 300, replace: 'Booting [====================================] 100%'},
        {delay: 300, line: ' &nbsp;'},
    ];

    var nextLine = function() {
        line = lines.shift();

        if (line.line) {
            $('#booting-text').append('<p>'+line.line+'</p>');
        } else if (line.replace) {
            $('#booting-text p:last').html('<p>'+line.replace+'</p>');
        }

        $('html, body').animate({ scrollTop: $(document).height() }, 0);

        if (lines.length) {
            setTimeout(nextLine, Math.round( line.delay * Math.random() ) );

        } else if (RED) {
            window.location.href = '/errors_screen'

        } else if (BLUE) {
            window.location.href = '/help_screen'

        }
    }

    // $('#booting-text').html('');
    // nextLine();

    server.eventListener(function(evt) {
        if (evt.type == 'boot') {
            $('#booting-text').html('');
            nextLine();

            play_sounds = false;

        } else if (evt.type == 'timer:start') {
            play_sounds = true;
        }
    });

    var play_sounds = false;

    $interval(function() {
        $('.blink').addClass('invisible');

        setTimeout(function() {

            /*
            if (play_sounds) {
                sounds.play('buzzer');
            }
            */

            $('.blink').removeClass('invisible');
        }, 250);
    }, 1000);

}]);