var app = angular.module('app', ['chart.js'])
.factory('server', ['$http', 'sounds', function($http, sounds) {

    if (GREEN) {
        var base_url = 'http://192.168.1.200/';
    } else {
        var base_url = 'http://gold.odyssey.app/';
    }
    // var base_url = 'http://escape-computer.dev/';

    /*
     *  Perform a GET request
     */
    function get(path, _data) {
        var data = _data || {};

        data.system = SYSTEM;
        data.cache = Math.random() * 10000;

        var params = [];
        for (var key in data) {
            params.push( key+'='+data[key] );
        }
        var query = params.join('&');

        return $http.get(base_url+path+'?'+query);
    }

    /*
     *  Perform a POST request
     */
    function post(path, data) {
        return $http.post(base_url+path+'?system='+SYSTEM, data);
    }

    var server = {
        // List events
        events: function(limit, callback) {
            get('events/'+limit).success(callback);
        },
        // Listen for events
        eventListener: function(callback) {
            function makeCall() {
                console.log('Polling for events...');

                get('events', {

                    timeout: 30000

                }).success(function(evt) {
                    if (evt) {
                        if (evt.type == 'error:activated') {
                            sounds.play('danger');
                        }

                        if (evt.type == 'ping') {
                            setTimeout(function() {
                                server.fireEvent('ping-response:'+SYSTEM);
                            }, 100);
                        }

                        callback(evt);
                    }

                    makeCall();
                });
            };

            makeCall();
        },
        // Fire an event
        fireEvent: function(type, message) {
            return post('events', {type: type, message: message});
        },
        // List errors
        errors: function(callback, active) {
            get('errors'+(active ? '/1': '')).success(callback);
        },
        // Activate an error
        activateError: function(code) {
            return post('errors/activate', {
                code: code
            });
        },
        // Resolve an error
        resolveError: function(code) {
            return post('errors/resolve', {
                code: code
            });
        },
    };

    return server;

}])
.factory('sounds', function() {
    var audio = $('#audio')[0];

    return {
        // Play a sound
        play: function(snd, voice) {

            var sounds = {
                'danger': 'woop.wav',
                'resolved': 'Computer_Data_06.wav',
                'failed': 'Alarm.wav',
                'evacuate': 'European_Siren.wav',
                'buzzer': 'buzzer.wav',
                // 'type': 'type_1.wav',

                'type': 'Computer_Data_01.wav',
                'correct': 'Computer_Data_03.wav',
                'incorrect': 'Computer_Data_05.wav',

                'oiss_active': 'Computer_Data_02.wav',
                'oiss_request': 'Computer_Data_05.wav',
            };

            try {
                if (audio) {
                    if (voice) {
                        audio.src = '/sounds/voices/'+snd;

                    } else if (sounds[snd]) {
                        audio.src = '/sounds/'+sounds[snd];

                    } else {
                        return;
                    }

                    audio.play();
                } else {
                    // alert('No audio!!');
                }
            } catch(e) {
                alert(e);
                console.error(e);
            }

        }
    }
})

.directive('sound', function() {
    return {
        restrict: 'E',
        template: '<div id="enable-sounds" ng-if="!sound_enabled">\
                <button class="btn btn-large btn-primary" ng-click="enableSound()">Press enter to enable sounds</button>\
            </div>\
            <audio id="audio" class="hidden"></audio>',
        controller: ['$rootScope', '$scope', 'sounds', function($rootScope, $scope, sounds) {
            setTimeout(function() {
                $('#enable-sounds button').focus();
            }, 1000);

            $scope.enableSound = function() {
                sounds.play('danger');

                $rootScope.sound_enabled = true;
            }
        }]
    };
});


/*
.directive('blink', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            setInterval(function() {
                $(element).addClass('invisible');
                setTimeout(function() {
                    $(element).removeClass('invisible');
                }, 250);
            }, 1000);
        }
    };
});
*/
