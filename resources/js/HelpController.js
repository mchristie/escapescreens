app.controller('HelpController', ['$scope', '$timeout', '$interval', 'server', 'sounds', function($scope, $timeout, $interval, server, sounds) {

    var unit = 100,
        canvas, context, canvas2, context2,
        height, width, xAxis, yAxis,
        draw,
        dir = 1, diff = 0.1,

        speeds = [
            {
                speed: 20,
                dir: false,
            },
            {
                speed: 15,
                dir: true,
            },
            {
                speed: 10,
                dir: false,
            },
            {
                speed: 5,
                dir: true,
            },
            {
                speed: 8,
                dir: false,
            }
        ];

    $scope.state    = 'idle';
    $scope.flicker  = false;
    $scope.message  = '';
    $scope.question = '';
    $scope.prompt   = 'Ask O.I.S.S. a question...';

    /*
     *  Lock down stuff
     */

    $scope.showing_exit_code    = false;
    $scope.showing_lockdown     = false;
    $scope.showing_launched     = false;
    $scope.showing_failed       = false;

    $scope.code_one             = '';
    $scope.code_two             = '';
    $scope.code_three           = '';
    $scope.actual_code_one      = '3C4D4J6I';
    $scope.actual_code_two      = '28617CYE';
    $scope.actual_code_three    = 'GALAXY';
    $scope.code_one_done        = false;
    $scope.code_two_done        = null;
    $scope.code_three_done      = null;

    $scope.checkCode = function(code) {

        if ($scope.code_one_done === false) {
            if ($scope.code_one.toUpperCase() == $scope.actual_code_one) {
                $scope.code_one_done = true;
                $scope.code_two_done = false;
                sounds.play('correct');
                setTimeout(function() {
                    $('#code_two').focus();
                }, 500);
            }
        }

        if ($scope.code_two_done === false) {
            if ($scope.code_two.toUpperCase() == $scope.actual_code_two) {
                $scope.code_two_done = true;
                $scope.code_three_done = false;
                sounds.play('correct');
                setTimeout(function() {
                    $('#code_three').focus();
                }, 500);
            }
        }

        if ($scope.code_three_done === false) {
            if ($scope.code_three.toUpperCase() == $scope.actual_code_three) {
                $scope.code_three_done = true;
                sounds.play('correct');
                server.fireEvent('exit');
            }
        }
    }

    /*
     *  Help stuff
     */

    function flicker() {
        $timeout(function() {
            $scope.flicker = !$scope.flicker;
            console.log($scope.flicker ? 'Flicker on' : 'Flicker off');

            flicker();
        }, Math.round(Math.random() * 10000));
    }
    flicker();

    var all_errors;
    server.errors(function(errors) {
        all_errors = errors;
    });

    $('#question').focus();

    $scope.ask = function() {
        server.fireEvent('help:request', $scope.question);

        $scope.question = '';
    }

    var handling_event  = false;
    var queued_events   = [];
    var queue_protection;
    var dont_queue = [
        'ping',
        'clear',
        'reset',
        'boot',
        'help:request',
        'help:active',
        'help:voice',
        'lockdown',
        'exit',
        'timer',
        'timer:start',
        'all_errors_resolved',
        'launched',
    ];

    var dont_block = [
        'ping',
        'clear',
        'reset',
        'boot',
        'help:request',
        'help:active',
        'help:voice',
        'timer',
        'timer:start',
    ];

    var reset_events = [
        'all_errors_resolved',
        'launched',
    ];

    var eventHandler = function(evt) {

        if (handling_event && dont_queue.indexOf(evt.type) === -1) {
            queued_events.push( evt );
            return;
        }

        if (dont_block.indexOf(evt.type) === -1) {
            handling_event = true;
        }

        if (reset_events.indexOf(evt.type) !== -1) {
            queued_events = [];
        }

        if (queue_protection) {
            $timeout.cancel(queue_protection);
        }
        $timeout(function() {
            handling_event = false;
            if (queued_events.length) {
                eventHandler( queued_events.pop() );
            }
        }, 30000);

        console.log(evt);


        if (evt.type == 'launched') {
            $scope.showing_lockdown     = false;
            $scope.showing_exit_code    = false;
            $scope.showing_launched     = true;
            $scope.showing_failed       = false;
            handling_event              = false;

        } else if (evt.type == 'failed') {

            $scope.showing_lockdown     = false;
            $scope.showing_exit_code    = false;
            $scope.showing_failed       = true;
            sounds.play('brace_for_impact.wav', true);

        } else if (evt.type == 'reset') {
            window.location.href = '/booting_screen';
            handling_event = false;
            return;

        } else if (evt.type == 'help:request') {
            $scope.state = 'idle';
            $scope.message = 'Processing...';

            sounds.play('oiss_request');

        } else if (evt.type == 'help:idle') {
            $scope.state = 'idle';
            $scope.message = '';

        } else if (evt.type == 'help:intermittent') {
            $scope.state = 'intermittent';
            $scope.message = '';

        } else if (evt.type == 'help:offline') {
            $scope.state = 'offline';

            if (parseInt(evt.message)) {
                $imeout(function() {
                    $scope.state = 'idle';
                }, parseInt(evt.message) * 1000)
            }

        } else if (
            evt.type == 'all_errors_resolved' ||
            evt.type == 'help:active' ||
            evt.type == 'help:voice' ||
            evt.type == 'error:activated' ||
            evt.type == 'error:resolved' ||
            evt.type == 'lockdown' ||
            evt.type == 'exit' ||
            evt.type == 'launching') {

            $scope.state = 'active';

            if (evt.type == 'help:active') {
                sounds.play('oiss_active');

            } else if (evt.type == 'error:activated') {
                // sounds.play('danger');

            } else if (evt.type == 'error:resolved') {
                // sounds.play('resolved');

            } else if (evt.type == 'all_errors_resolved') {
                message = "All errors have been resolved, enter the eight digit code on the right hand keyboard to initiate the launch procedure";
                $scope.prompt = "Enter eight digit code";
                sounds.play('errors_resolved.wav', true);
            }

            var message;
            if (evt.type == 'help:active') {
                message = evt.message;

            } else if (evt.type == 'exit') {

                $scope.showing_lockdown = false;
                $scope.showing_exit_code = true;
                sounds.play('manual_override_successful.wav', true);

            } else if (evt.type == 'lockdown') {
                $scope.showing_lockdown = true;
                message = 'The launch process has been put on hold until the problem in the engine room has been resolved';
                sounds.play( 'engine_room.wav', true );

                setTimeout(function() {
                    $('#code_one').focus();
                }, 2000);

            } else {

                if (evt.type == 'help:voice') {
                    sounds.play( evt.message, true );
                    var messages = {
                        'even_trying.wav'       : "Are you even trying, cadet?",
                        'lives_at_risk.wav'     : "You know there are lives at risk here, right?",
                        'look_closely.wav'      : "You'll have to look very, very closely.",
                        'not_right.wav'         : "That doesn't sound right to me.",
                    }
                    message = messages[evt.message];
                }

                if (evt.type == 'launching') {
                    sounds.play('code_affirmative.wav', true);
                    message = 'All errors are now resolved, launch process has been initiated';
                }

                angular.forEach(all_errors, function(err) {
                    if (err.code == evt.message) {

                        if (evt.type == 'error:activated') {
                            message = err.level.substr(0, 1).toUpperCase() + err.level.substr(1) + ' - ' + (err.description || err.title);

                            if (err.activate_sound) {
                                sounds.play(err.activate_sound, true);
                            } else {
                                sounds.play('danger');
                            }

                            // If there's no power turn LOISS off
                            if (err.code == 'no_power') {
                                setTimeout(function() {
                                    server.fireEvent('help:offline');
                                }, 15000);
                            }

                        } else if (evt.type == 'error:resolved') {
                            message = err.resolve_title;

                            if (err.resolve_sound) {
                                sounds.play(err.resolve_sound, true);
                            } else {
                                sounds.play('resolved');
                            }
                        }
                    }
                });
            }

            var letters = 1;
            var typing_interval = $interval(function() {
                // Add a letter on if we have more to type
                if (message.length >= letters) {
                    $scope.message = message.substr(0, letters);
                    letters++;

                // If were at the end, set a timeout to clear it after 20 seconds
                } else {
                    $timeout(function() {
                        $scope.message = '';
                        $scope.state = 'idle';
                        $scope.$apply();

                        handling_event = false;
                        if (queued_events.length) {
                            eventHandler( queued_events.pop() );
                        }

                    }, 20000);
                    $interval.cancel(typing_interval);
                }

            }, 75);

        }
    }
    server.eventListener(eventHandler);

    /**
     * Init function.
     *
     * Initialize variables and begin the animation.
     */
    function init() {

        canvas = document.getElementById("sine");

        canvas.width = window.outerWidth;
        canvas.height = $('#sine-container').outerHeight() - 100;

        context = canvas.getContext("2d");
        context.strokeStyle = '#000';
        context.lineJoin = 'round';
        context.lineWidth = 4;

        height = canvas.height;
        width = canvas.width;

        xAxis = Math.floor(height/2);
        // yAxis = Math.floor(width/4);
        yAxis = 0;

        context.save();
        draw();
    }

    /**
     * Draw animation function.
     *
     * This function draws one frame of the animation, waits 20ms, and then calls
     * itself again.
     */
    draw = function () {

        // Clear the canvas
        context.clearRect(0, 0, width, height);

        // Draw the axes in their own path
        context.beginPath();
        // drawAxes();
        // context.stroke();

        for (var i = 0; i < speeds.length; i++) {
            if (speeds[i].dir) {
                speeds[i].speed += Math.ceil( Math.random() * (i/2 + 1));

            } else {
                speeds[i].speed -= Math.ceil( Math.random() * (i/2 + 1));
            }

            if (speeds[i].speed >= 40) {
                speeds[i].dir = false;
            }
            if (speeds[i].speed <= 5) {
                speeds[i].dir = true;
            }
        }

        // Set styles for animated graphics
        context.save();

        for (var i = 0; i < speeds.length; i++) {

            if (($scope.state == 'idle' || $scope.state == 'intermittent') && i > 0 && i < 3) {
                continue;
            }

            unit = speeds[i].speed * 5;

            // context.strokeStyle = '#68E6FD';
            context.strokeStyle = 'rgba(104, 230, 253, '+(5-i)/15+')';
            context.fillStyle = '#fff';
            context.lineWidth = 4;

            // Draw the sine curve at time draw.t, as well as the circle.
            context.beginPath();
            drawSine(draw.t);
            context.stroke();

            context.restore();

        }

        // Draw the xAxis PI tick and the time
        // context.fillText("Ï�", xAxis + 59+3*unit, 18+xAxis);
        // context.fillText("t = "+Math.floor(Math.abs(draw.seconds)), 10, 20);

        // Update the time and draw again
        draw.seconds = draw.seconds - .07;
        draw.t = draw.seconds*Math.PI;
        setTimeout(draw, 50);
    };
    draw.seconds = 0;
    draw.t = 0;

    /**
     * Function to draw sine
     *
     * The sine curve is drawn in 10px segments starting at the origin.
     */
    function drawSine(t) {

        // Set the initial x and y, starting at 0,0 and translating to the origin on
        // the canvas.
        var x = t;
        var y = Math.sin(x);
        context.moveTo(yAxis, unit*y+xAxis);

        // Loop to draw segments
        for (i = yAxis; i <= width; i += 10) {
            x = t+(-yAxis+i)/unit;
            y = Math.sin(x);
            context.lineTo(i, unit*y+xAxis);
        }
    }

    init()

}]);