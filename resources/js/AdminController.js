app.controller('AdminController', ['$scope', '$timeout', '$interval', 'server', 'sounds', function($scope, $timeout, $interval, server, sounds) {

    $scope.time             = '60:00';
    $scope.seconds          = 3600;
    $scope.timing           = false;
    var start_time, timer;

    $scope.status_blue      = false;
    $scope.status_red       = false;
    $scope.status_green     = false;
    $scope.status_gold      = false;

    $scope.webcam           = false;
    $scope.video_message    = false;

    $scope.highlight_events = [
        'help:request',
        'gncs:correct',
        'gncs:incorrect',
    ];

    $scope.startTimer = function() {
        $scope.timing = true;
        start_time = Math.round(new Date().getTime() / 1000);

        server.fireEvent('timer:start');

        timer = $interval(function() {
            var now = Math.round(new Date().getTime() / 1000);

            var seconds = (60 * 60) - (now - start_time);
            $scope.seconds = seconds;

            // console.log(seconds);

            var current_seconds = seconds;
            var minutes = 0;
            while (current_seconds >= 60) {
                minutes++;
                current_seconds -= 60;
            }

            if (seconds == 3599) {
                // server.fireEvent('oxygen:draining');

            } else if (seconds == 1800) {
                server.fireEvent('oxygen:30');

            } else if (seconds == 300) {
                server.fireEvent('oxygen:5');

            } else if (seconds == 60) {
                server.fireEvent('oxygen:1');

            } else if (seconds <= 0) {
                // alert('Time is up!');
                server.fireEvent('oxygen:gone');
                $interval.cancel(timer);

            }

            if (seconds % 60 == 0) {
                server.fireEvent('timer', minutes - 1);
            }

            $scope.time = (minutes < 10 ? '0'+minutes : minutes)+':'+(current_seconds < 10 ? '0'+current_seconds : current_seconds);
        }, 1000);

        console.log(timer);
    }

    $scope.stopTimer = function() {
        $scope.timing = false;
        $interval.cancel(timer);
    }

    $scope.changeTimer = function() {
        var new_time = prompt('Enter a new time, or cancel. (Format must be mintues:seconds) ', $scope.time);

        if (new_time) {
            var parts = new_time.split(':');

            var secs = 3600 - (parseInt(parts[0]) * 60) + parseInt(parts[1]);
            start_time = Math.round(new Date().getTime() / 1000) - secs;
        }
    }

    /*
     *  Errors
     */

    $scope.toggleError = function(error) {
        if (error.status == 1) {
            // if (confirm('Resolve error: '+error.title+'?')) {
                server.resolveError(error.code).success( $scope.updateErrors );
            // }
        } else {
            // if (confirm('Activate error: '+error.title+'?')) {
                server.activateError(error.code).success( $scope.updateErrors );
            // }
        }
    }

     // Update the errors list
    $scope.updating_errors = false;
    $scope.updateErrors = function() {
        $scope.updating_errors = true;
        server.errors(function(errors) {
            $scope.updating_errors = false;
            $scope.errors = errors;
        });
    }
    $scope.updateErrors();

    server.eventListener(function(evt) {
        if (evt.type == 'reset') {
            window.location.reload();
            return;
        }

        if (evt.type == 'tensey_tense') {
            sounds.play('tensey_tense.m4a', true);

        } else if (evt.type == 'boot') {
            // sounds.play('hello_cadets.wav', true);
            sounds.play('ambience_and_hello.m4a', true);

        } else if (evt.type == 'launched') {
            sounds.play('launched_odyssey.m4a', true);

        } else if (evt.type == 'lockdown') {
            sounds.play('meteor_shower.m4a', true);

        } else if (evt.type == 'exit') {
            sounds.play('exit.m4a', true);
        }

        if (evt.type == 'ping') {
            $scope.status_blue = false;
            $scope.status_red = false;
            $scope.status_green = false;
            $scope.status_gold = false;
        }
        if (evt.type.substr(0, 14) == 'ping-response:') {
            $scope['status_'+evt.type.replace('ping-response:', '')] = true;
        }

        if (evt.type == 'help:request' && evt.message == '27831744') {
            server.fireEvent('launching');
        }

        $scope.events.unshift( evt );

        if ($scope.events.length > 10) {
            $scope.events.pop();
        }

        $scope.updateErrors();
    });

    $scope.updateEvents = function() {
        $scope.events = ['', '', '', '', ''];
        server.events(10, function(events) {
            $scope.events = events;
            while ($scope.events.length < 10) {
                $scope.events.push({title: '-'});
            }
        });
    };
    $scope.updateEvents();

    $scope.fire_event = '';
    $scope.event_message = '';
    $scope.fireEvent = function(evt) {
        if (evt) {
            $scope.fire_event = evt;
        }

        if ($scope.fire_event == 'reset' && !confirm('Really, reset all screens?')) {
            $scope.$apply(function() {
                $scope.fire_event = '';
                $scope.event_message = '';
            });
            return;
        }

        if ($scope.fire_event == 'webcam:activate') {
            $scope.webcam = true;

        } else if ($scope.fire_event == 'webcam:deactivate') {
            $scope.webcam = false;

        } else if ($scope.fire_event == 'video_message:start') {
            $scope.video_message = true;

        } else if ($scope.fire_event == 'video_message:stop') {
            $scope.video_message = false;
        }

        if ($scope.fire_event.indexOf('|') !== -1) {
            var parts = $scope.fire_event.split('|');

            $scope.fire_event       = parts[0];
            $scope.event_message    = parts[1];
        }

        server.fireEvent($scope.fire_event, $scope.event_message).success(function() {
            $scope.fire_event = '';
            $scope.event_message = '';
        });

    };

    $timeout(function() {
        server.fireEvent('ping');
    }, 1000);


}]);