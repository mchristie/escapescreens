app.controller('TabletController', ['$scope', 'server', 'sounds', '$timeout', function($scope, server, sounds, $timeout) {

    $scope.code = [null,null,null,null,null];

    var correct_code = '86880';

    $scope.status = 'idle';

    $scope.showing_image = false;

    // idle
    // verifying
    // incorrect
    // correct

    var box = 9;

    $scope.focus = function(_box) {
        box = _box;

        $scope.code[box] = '';

        $('input').removeClass('danger');
        $('#input_'+box).addClass('danger');
    }

    $scope.btn = function(btn) {

        if (btn == 'clear') {
            $scope.code = [null,null,null,null,null];
            $scope.focus(0);

            $scope.status = 'idle';

        } else if (btn == 'submit') {
            var code = '' + $scope.code[0] + $scope.code[1] + $scope.code[2] + $scope.code[3] + $scope.code[4];
            console.log(code);

            if (code == correct_code) {
                $scope.status = 'correct';
                sounds.play('correct');

                server.fireEvent('gncs:correct', code);

                $timeout(function() {
                    $scope.showing_image = true;
                }, 3000);
            } else {
                $scope.status = 'incorrect';
                sounds.play('incorrect');

                $scope.code = [null,null,null,null,null];
                $scope.focus(0);

                server.fireEvent('gncs:incorrect', code);
            }

        } else {
            sounds.play('type');

            if (box >= 5) {
                $scope.code = [null,null,null,null,null];
                box = 0;
            }

            $scope.code[box] = btn;
            box++;

            $scope.focus( box );

            $scope.status = 'idle';

            if (box == 5) {
                $scope.btn('submit');
            }
        }

        setTimeout(function() {
            $('body').focus();
        }, 500);
    }

    $scope.focus(0);


    /*
     *  Reset the tablet when needed
     */
    server.eventListener(function(evt) {
        if (evt.type == 'reset') {
            window.location.reload();
            return;
        }
    });

}]);