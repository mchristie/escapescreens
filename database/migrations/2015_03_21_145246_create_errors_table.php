<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErrorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('errors', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('display')->nullable();
			$table->boolean('status');
			$table->boolean('default');
			$table->string('code');
			$table->string('level');
			$table->string('title');
			$table->string('description')->nullable();
			$table->string('resolve_title')->nullable();
			$table->string('activate_sound')->nullable();
			$table->string('resolve_sound')->nullable();
			$table->string('image')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('errors');
	}

}
