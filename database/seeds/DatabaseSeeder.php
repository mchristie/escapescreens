<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('events')->truncate();
        DB::table('errors')->truncate();

        $errors = [
            [
                'display'           => 1,
                'code'              => 'propulsion',
                'status'            => 1,
                'default'           => 1,
                'level'             => 'danger',
                'title'             => 'Propulsion Error',
                'resolve_title'     => 'The propulsion systems have been safely corrected',
                'activate_sound'    => null,
                'resolve_sound'     => 'propulsion.wav',
                'image'             => 'Reactor_error_pic.png',
            ],
            [
                'display'           => 1,
                'code'              => 'lunar_buggy',
                'status'            => 1,
                'default'           => 1,
                'level'             => 'warning',
                'title'             => 'Lunar Buggy Distress Call',
                'resolve_title'     => 'The Lunar Buggy distress call has been resolved',
                'activate_sound'    => null,
                'resolve_sound'     => 'lunar_resolved.wav',
                'image'             => 'Lunar_buddy_call_pic.png',
            ],
            [
                'display'           => 1,
                'code'              => 'command_data',
                'status'            => 1,
                'default'           => 1,
                'level'             => 'danger',
                'title'             => 'Command & Data Handling System',
                'resolve_title'     => 'The Command & Data Handling System have been repaired',
                'activate_sound'    => null,
                'resolve_sound'     => 'command_resolved.wav',
                'image'             => 'Command_error_pic.png',
            ],
            [
                'display'           => 1,
                'code'              => 'attitude',
                'status'            => 1,
                'default'           => 1,
                'level'             => 'danger',
                'title'             => 'Attitude Control',
                'resolve_title'     => 'The Attitude Control systems have been repaired',
                'activate_sound'    => null,
                'resolve_sound'     => 'attitude_resolved.wav',
                'image'             => 'Attitude_Control_Error.png',
            ],
            [
                'display'           => 1,
                'code'              => 'life_support',
                'status'            => 1,
                'default'           => 1,
                'level'             => 'danger',
                'title'             => 'Life Support System',
                'resolve_title'     => 'The Life Support System are now operating within safe parameters',
                'activate_sound'    => null,
                'resolve_sound'     => 'life_support_resolved.wav',
                'image'             => 'Life_support_pic.png',
            ],
            [
                'display'           => 1,
                'code'              => 'guidance',
                'status'            => 1,
                'default'           => 1,
                'level'             => 'warning',
                'title'             => 'G.N.C.S. System',
                'resolve_title'     => 'The Guidance, Navigation & Control (G.N.C.S.) System are now operating',
                'activate_sound'    => null,
                'resolve_sound'     => 'gnc_resolved.wav',
                'image'             => 'GNC_Error_pic.png',
            ],
            [
                'display'           => 1,
                'code'              => 'thermal',
                'status'            => 1,
                'default'           => 1,
                'level'             => 'danger',
                'title'             => 'Thermal Control',
                'resolve_title'     => 'The Thermal Control systems are now functioning correctly',
                'activate_sound'    => null,
                'resolve_sound'     => 'thermal_resolved.wav',
                'image'             => 'Thermal_Control_Error_pic.png',
            ],
            [
                'display'           => 1,
                'code'              => 'thermoelectric',
                'status'            => 1,
                'default'           => 1,
                'level'             => 'warning',
                'title'             => 'Thermoelectric Generator',
                'resolve_title'     => 'The Thermoelectric Generator is now functioning correctly',
                'activate_sound'    => null,
                'resolve_sound'     => 'thermo_electric_resolved.wav',
                'image'             => 'Thermo_electric_generator_pic.png',
            ],
            [
                'display'           => 0,
                'code'              => 'no_power',
                'status'            => 0,
                'default'           => 0,
                'level'             => 'danger',
                'title'             => 'No power',
                'description'       => 'Insufficient power remaining to run L.O.I.S.S. - shutting down',
                'resolve_title'     => null,
                'activate_sound'    => 'no_power.wav',
                'resolve_sound'     => null,
                'image'             => 'Reactor_error_pic.png',
            ],
            [
                'display'           => 0,
                'code'              => 'engine_room',
                'status'            => 0,
                'default'           => 0,
                'level'             => 'danger',
                'title'             => 'Engine room',
                'description'       => 'The launch process has been put on hold until the problem in the engine room has been resolved',
                'resolve_title'     => null,
                'activate_sound'    => 'engine_room.wav',
                'resolve_sound'     => 'final_clue.wav',
                'image'             => null,
            ],


            [
                'display'           => 0,
                'code'              => 'oxygen:draining',
                'status'            => 0,
                'default'           => 0,
                'level'             => 'warning',
                'title'             => 'Oxygen levels are dropping rapidly, less than 1 hour remaining.',
                'description'       => 'Oxygen levels are dropping rapidly, less than 1 hour remaining.',
                'resolve_title'     => null,
                'activate_sound'    => null,
                'resolve_sound'     => null,
                'image'             => null,
            ],
            [
                'display'           => 0,
                'code'              => 'oxygen:30',
                'status'            => 0,
                'default'           => 0,
                'level'             => 'warning',
                'title'             => 'Less than 30 minutes of oxygen remaining.',
                'description'       => 'Less than 30 minutes of oxygen remaining.',
                'resolve_title'     => null,
                'activate_sound'    => '30_minutes.wav',
                'resolve_sound'     => null,
                'image'             => null,
            ],
            [
                'display'           => 0,
                'code'              => 'oxygen:15',
                'status'            => 0,
                'default'           => 0,
                'level'             => 'danger',
                'title'             => 'Oxygen levels critical - less than 15 minutes remaining.',
                'description'       => 'Oxygen levels critical - less than 15 minutes remaining.',
                'resolve_title'     => null,
                'activate_sound'    => '15_minutes.wav',
                'resolve_sound'     => null,
                'image'             => null,
            ],
            [
                'display'           => 0,
                'code'              => 'oxygen:5',
                'status'            => 0,
                'default'           => 0,
                'level'             => 'danger',
                'title'             => 'Oxygen levels critical - less than 5 minutes remaining.',
                'description'       => 'Oxygen levels critical - less than 5 minutes remaining.',
                'resolve_title'     => null,
                'activate_sound'    => '5_minutes.wav',
                'resolve_sound'     => null,
                'image'             => null,
            ],
            [
                'display'           => 0,
                'code'              => 'oxygen:1',
                'status'            => 0,
                'default'           => 0,
                'level'             => 'danger',
                'title'             => 'Oxygen reserves depleted - less than 1 minute remaining.',
                'description'       => 'Oxygen reserves depleted - less than 1 minute remaining.',
                'resolve_title'     => null,
                'activate_sound'    => null,
                'resolve_sound'     => null,
                'image'             => null,
            ],
            [
                'display'           => 0,
                'code'              => 'oxygen:gone',
                'status'            => 0,
                'default'           => 0,
                'level'             => 'danger',
                'title'             => 'No oxygen remaining - game over.',
                'description'       => 'No oxygen remaining - game over.',
                'resolve_title'     => null,
                'activate_sound'    => null,
                'resolve_sound'     => null,
                'image'             => null,
            ]
        ];

        foreach ($errors as $error) {
            \App\Models\Error::create($error);
        }

        // $this->call('UserTableSeeder');
    }

}
